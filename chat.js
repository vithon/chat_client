LOCAL_MODE = document.location.protocol == "file:";
SERVER_URL = "server.html";
VK_UP = 38;
VK_DOWN = 40;
VK_RETURN = 13;

function encodeUri(params) {
	var ret = [];
	for (key in params) {
		ret.push(escape(key) + "=" + encodeURIComponent(params[key]));
	}
	return ret.join('&');
}


function ChatView(element) {
	this.element = element;

	this.initialize = function() {
		this.chatArea = document.createElement('div');
		this.chatArea.className = "chatMessageDisplay";
		this.element.appendChild(this.chatArea);
	}

	this.appendMessage = function(sender, content, headerStyle) {
		var messageTL = document.createElement('div');
		messageTL.className = "chatMessageTopLeft";
		var messageTR = document.createElement('div');
		messageTR.className = "chatMessageTopRight";
		var header = document.createElement('div');
		header.className = headerStyle;
		var headerLeft = document.createElement('div');
		headerLeft.className = "chatHeaderLeft";
		var headerRight = document.createElement('div');
		headerRight.className = "chatHeaderRight";

		var author = document.createElement('div');
		author.className = "chatAuthor";
		var authorText = document.createTextNode(sender);
		author.appendChild(authorText);

		messageTL.appendChild(messageTR);
		messageTR.appendChild(header);
		header.appendChild(headerLeft);
		headerLeft.appendChild(headerRight);
		headerRight.appendChild(author);

		var messageBL = document.createElement('div');
		messageBL.className = "chatMessageBottomLeft";
		var messageBR = document.createElement('div');
		messageBR.className = "chatMessageBottomRight";

		var message = document.createElement('div');
		message.className = 'chatMessage';
		var messageText = document.createTextNode(content);
		message.appendChild(messageText);

		messageTR.appendChild(messageBL);
		messageBL.appendChild(messageBR);
		messageBR.appendChild(message);

		this.chatArea.appendChild(messageTL);
		this.chatArea.scrollTop = this.chatArea.scrollHeight;
	}

	this.appendSelf = function(author, message) {
		this.appendMessage(author, message, 'chatHeaderSelf');
	}

	this.appendOther = function(author, message) {
		this.appendMessage(author, message, 'chatHeaderOther');
	}

}


function ChatInput(element) {
	this.element = element;
	this.listeners = [];

	this.initialize = function() {
		var form = document.createElement('form');
		form.className = 'chatMessageForm';
		this.input = document.createElement('input');
		this.input.type = 'text';
		this.input.className = 'chatMessageInput';
		this.input.myself = this;
		this.input.onkeydown = this.onKeyDown;
		form.appendChild(this.input);
		this.element.appendChild(this.input);
	}

	this.addKeyListener = function(callback) {
		this.listeners.push(callback);
	}

	this.onKeyDown = function(evt) {
		if (evt.target == this) {
			for (var i = this.myself.listeners.length - 1; i >= 0; i--) {
				var ret = this.myself.listeners[i](evt, this.myself);
				if (ret == false) {
					return false;
				}
			}
		}
		return true;
	}

	this.getValue = function() {
		return this.input.value;
	}

	this.setValue = function(msg) {
		this.input.value = msg;
	}

}


function ChatController(viewingArea, typingArea) {
	this.view = new ChatView(viewingArea);
	this.input = new ChatInput(typingArea);
	// assigned in this.start
	this.serverUrl = null;
	this.messages = [];
	this.currentIndex = 0;
	this.messageCount = 0;
	this.refreshTime = null;
	this.author = 0;

	this.start = function(author, serverUrl) {
		this.author = author;
		this.serverUrl = serverUrl || SERVER_URL;
		this.view.initialize();
		this.input.initialize();
		this.input.controller = this;
		this.input.addKeyListener(this.onKeyDown);
		this.installRefreshListener();
	}

	this.installRefreshListener = function() {
		this.refreshTime = (new Date()).getTime();
		this.refresh.periodical(2000, this, 2000);
	}

	this.refresh = function(interval) {
		var now = (new Date()).getTime();
		if (now < this.refreshTime + interval) {
			return;
		}
		this.refreshTime = now;
		var ajax = new XHR({
			onSuccess: this.refreshSuccess,
			isSuccess: function(status) {
				return status == 0 || (status >= 200 && status < 300);
			} });
		ajax.controller = this;
		if (!LOCAL_MODE) {
			ajax.send(this.serverUrl, encodeUri({'action': 'update', 'offset': this.messageCount}));
		} else {
			ajax.send(this.serverUrl + this.messageCount);
		}
	}

	this.refreshSuccess = function(text, xml) {
		var me = this.controller;
		var lines = text.split(/\r?\n/);
		while (lines.length > 2) {
			var id = lines.shift();
			id = (new Number(id)).valueOf();
			var author = lines.shift();
			var msg = lines.shift();
			if (msg == undefined) {
				break;
			}
			if (id <= me.messageCount) {
				continue;
			}
			if (author == me.author) {
				me.view.appendSelf(author, msg);
			} else {
				me.view.appendOther(author, msg);
			}
			me.messageCount = id;
		}
	}

	this.onUp = function(src) {
		this.currentIndex--;
		if (this.currentIndex < 0) {
			this.currentIndex = 0;
		}
		var message = "";
		if (this.currentIndex < this.messages.length) {
			message = this.messages[this.currentIndex];
		}
		this.input.setValue(message);
	}

	this.onDown = function(src) {
		this.currentIndex++;
		var message = "";
		if (this.currentIndex < this.messages.length) {
			message = this.messages[this.currentIndex];
		} else {
			this.currentIndex = this.messages.length;
		}
		this.input.setValue(message);
	}

	this.sendMessage = function(message) {
		var ajax = new XHR({
			isSuccess: function(status) {
				return status == 0 || (status >= 200 && status < 300);
			} });
		if (!LOCAL_MODE) {
			var data = encodeUri({'action': 'send', 'message': message});
			ajax.send(this.serverUrl, data);
		} else {
			ajax.send(this.serverUrl + this.messageCount);
		}
	}

	this.onEnter = function(src) {
		var message = this.input.getValue();
		if (this.messages.length > 100) {
			this.messages.shift();
		}
		this.messages.push(message);
		this.currentIndex = this.messages.length;
		this.sendMessage(message);
		this.input.setValue("");
	}

	this.onKeyDown = function(evt, src) {
		if (evt.keyCode != VK_UP && evt.keyCode != VK_DOWN && evt.keyCode != VK_RETURN) {
			return true;
		}
		if (evt.keyCode == VK_UP) {
			src.controller.onUp(src);
			return false;
		} else if (evt.keyCode == VK_DOWN) {
			src.controller.onDown(src);
			return false;
		}
		src.controller.onEnter(src);
		return false;
	}

}
